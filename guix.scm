;;; guix.scm --- Guix package for Prometheus TP-Link Exporter

;; Copyright © 2022 Oleg Pykhalov <go.wigust@gmail.com>

;; This file is part of prometheus-tp-link-exporter.

;; prometheus-tp-link-exporter is free software; you can redistribute it
;; and/or modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; prometheus-tp-link-exporter is distributed in the hope that it will be
;; useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
;; Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with prometheus-tp-link-exporter.  If not, see
;; <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file contains Guix package for development version of
;; prometheus-tp-link-exporter.  To build or install, run:
;;
;;   guix build --file=guix.scm
;;   guix package --install-from-file=guix.scm

;; The main purpose of this file though is to make a development
;; environment for building prometheus-tp-link-exporter:
;;
;;   guix shell --pure

;;; Code:

(use-modules ((guix licenses) #:prefix license:)
             (gnu packages admin)
             (gnu packages monitoring)
             (guix build utils)
             (guix build-system python)
             (guix gexp)
             (guix git-download)
             (guix packages)
             (ice-9 popen)
             (ice-9 rdelim))

(define %source-dir (dirname (current-filename)))

(define (git-output . args)
  "Execute 'git ARGS ...' command and return its output without trailing
newspace."
  (with-directory-excursion %source-dir
    (let* ((port   (apply open-pipe* OPEN_READ "git" args))
           (output (read-string port)))
      (close-pipe port)
      (string-trim-right output #\newline))))

(define (current-commit)
  (git-output "log" "-n" "1" "--pretty=format:%H"))

(define-public prometheus-tp-link-exporter
  (let ((commit (current-commit)))
    (package
      (name "prometheus-tp-link-exporter")
      (version (string-append "1.0.0" "-" (string-take commit 7)))
      (source (local-file %source-dir
                          #:recursive? #t
                          #:select? (git-predicate %source-dir)))
      (build-system python-build-system)
      (arguments
       '(#:tests? #f)) ; no tests
      (propagated-inputs
       (list jc python-prometheus-client))
      (home-page "https://gitlab.com/wigust/prometheus-tp-link-exporter")
      (synopsis "Prometheus TP-Link Exporter")
      (description
       "This package provides Prometheus TP-Link Exporter.")
      (license license:gpl3+))))

prometheus-tp-link-exporter
