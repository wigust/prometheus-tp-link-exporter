"""Connect to TP-Link router over SSH and collect metrics."""

from prometheus_client import Counter, start_http_server
import jc.parsers.ifconfig
import logging
import os
import subprocess
import time

log = logging.getLogger("prometheus-tp-link-exporter")
host = os.getenv("PROMETHEUS_TP_LINK_EXPORTER_HOST", "")
password = os.getenv("PROMETHEUS_TP_LINK_EXPORTER_PASSWORD", "")
interval = int(os.getenv("PROMETHEUS_TP_LINK_EXPORTER_INTERVAL", "10"))
listen_addresses = os.getenv(
    "PROMETHEUS_TP_LINK_EXPORTER_LISTEN_ADDRESS", "0.0.0.0:9101"
)


def ifconfig_devices():
    """Connect to TP-Link device over SSH and return ifconfig output."""
    return jc.parsers.ifconfig.parse(
        subprocess.getoutput(
            f"sshpass -p{password} ssh -o KexAlgorithms=+diffie-hellman-group1-sha1 -F /dev/null admin@{host} /sbin/ifconfig"
        )
    )


node_network_receive_bytes_total = Counter(
    "node_network_receive_bytes_total",
    "Network device statistic receive_bytes.",
    ["device"],
)

node_network_transmit_bytes_total = Counter(
    "node_network_transmit_bytes_total",
    "Network device statistic transmit_bytes.",
    ["device"],
)


def process_node_network():
    """Process request."""
    devices = ifconfig_devices()
    for device in devices:
        if (device["name"],) in node_network_transmit_bytes_total._metrics:
            previos_value = node_network_transmit_bytes_total._metrics[
                (device["name"],)
            ]._value._value
            if previos_value < device["tx_bytes"]:
                log.debug(f"Previous value {previos_value} is bigger than current value {device['rx_bytes']} on device {device['name']}, skipping updating the value.")
                node_network_transmit_bytes_total.labels(device=device["name"]).inc(
                    device["tx_bytes"] - previos_value
                )
        else:
            node_network_transmit_bytes_total.labels(device=device["name"]).inc(device["tx_bytes"])
    for device in devices:
        if (device["name"],) in node_network_receive_bytes_total._metrics:
            previos_value = node_network_receive_bytes_total._metrics[(device["name"],)]._value._value
            if previos_value < device["rx_bytes"]:
                node_network_receive_bytes_total.labels(device=device["name"]).inc(
                    device["rx_bytes"] - previos_value
                )
            else:
                log.debug(f"Previous value {previos_value} is bigger than current value {device['rx_bytes']} on device {device['name']}, skipping updating the value.")
        else:
            node_network_receive_bytes_total.labels(device=device["name"]).inc(device["rx_bytes"])


def main():
    """Entry point."""
    start_http_server(
        port=int(listen_addresses.split(":")[1]), addr=listen_addresses.split(":")[0]
    )
    while True:
        process_node_network()
        time.sleep(interval)


if __name__ == "__main__":
    main()
